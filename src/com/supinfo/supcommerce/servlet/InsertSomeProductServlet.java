package com.supinfo.supcommerce.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.sun.supcommerce.bo.SupProduct;
import com.supinfo.sun.supcommerce.doa.SupProductDao;

@WebServlet("/auth/basicInsert")
public class InsertSomeProductServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1)
			throws ServletException, IOException {
		SupProduct supProduct = new SupProduct();
		supProduct.setName("name");
		supProduct.setPrice(3f);
		supProduct.setContent("content");
		
		SupProductDao.addProduct(supProduct);
	}

}
