package com.supinfo.supcommerce.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.dao.DaoFactory;
import com.supinfo.supcommerce.entity.Category;
import com.supinfo.supcommerce.entity.Product;

@WebServlet("/auth/addProduct")
public class AddProductServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.getSession().setAttribute("categories", DaoFactory.getCategoryDao().findAll());
		
		RequestDispatcher rd = req.getRequestDispatcher("/auth/addProduct.jsp");
		rd.include(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		Product product = new Product();
		product.setName(req.getParameter("name"));
		product.setContent(req.getParameter("content"));
		product.setPrice(Float.parseFloat(req.getParameter("price")));
		Long categoryId = Long.valueOf(req.getParameter("category.id"));
		Category category = DaoFactory.getCategoryDao().findById(categoryId);
		product.setCategory(category);
		DaoFactory.getProductDao().save(product);
				
		resp.sendRedirect(req.getContextPath() + "/auth/showProduct?id=" + product.getId());
	}

	
	
}
