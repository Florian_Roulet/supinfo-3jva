package com.supinfo.supcommerce.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.supcommerce.dao.DaoFactory;

@WebServlet("/auth/showProduct")
public class ShowProductServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String id = req.getParameter("id");
		req.getSession().setAttribute("product", DaoFactory.getProductDao().findProductById(Long.valueOf(id)));
						
		RequestDispatcher rd = req.getRequestDispatcher("/auth/showProduct.jsp");
		rd.include(req, resp);
	}
}
