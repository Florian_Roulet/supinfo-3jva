package com.supinfo.supcommerce.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supinfo.supcommerce.servlet.LoginServlet;

@WebFilter(filterName="authenticateFilter", urlPatterns={"/auth/*"})
public class AuthenticateFilter implements Filter {

	FilterConfig config;

	public void setFilterConfig(FilterConfig config) {
		this.config = config;
	}

	public FilterConfig getFilterConfig() {
		return config;
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		
		HttpServletResponse response = (HttpServletResponse) resp; 
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession();
		//ServletContext sc = config.getServletContext();
		String username = (String) session.getAttribute(LoginServlet.USERNAME);
		//System.out.println("username  : " + username);
		if (username == null || "".equals(username)) {
			response.sendRedirect(request.getContextPath() + "/");
		} else {
			chain.doFilter(req, resp);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		setFilterConfig(arg0);
	}

}
