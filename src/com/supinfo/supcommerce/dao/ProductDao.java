package com.supinfo.supcommerce.dao;

import java.util.List;

import com.supinfo.sun.supcommerce.exception.UnknownProductException;
import com.supinfo.supcommerce.entity.Product;

public interface ProductDao {
	
	public void save(Product product);
	
	/**
	 * Find a product by id
	 * @throws UnknownProductException when no product in memory have the id
	 * @param the id of the product to find
	 * @return the product corresponding to the id
	 */
	public Product findProductById(Long id);
	
	/**
	 * @return an unmodifiable list of all products stored in memory
	 */
	public List<Product> getAllProducts() ;
	/**
	 * Remove a product from the memory
	 * @param the product to remove
	 * @throws UnknownProductException when the product doesn't exist in memory
	 */
	public void removeProduct(Product product);
	
	/**
	 * Remove a product from the memory
	 * @param the id of the product to remove
	 * @throws UnknownProductException when the product doesn't exist in memory
	 */
	public void removeProduct(Long id);
	
}
