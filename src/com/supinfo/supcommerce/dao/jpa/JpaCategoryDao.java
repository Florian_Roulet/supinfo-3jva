package com.supinfo.supcommerce.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.supinfo.supcommerce.dao.CategoryDao;
import com.supinfo.supcommerce.entity.Category;

public class JpaCategoryDao implements CategoryDao {

	private EntityManagerFactory emf;

	public JpaCategoryDao(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public Category findById(Long id) {
		Category category = null;
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			category = em.find(Category.class, id);
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
		return category;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAll() {
		List<Category> categories = null;
		EntityManager em = emf.createEntityManager();
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			categories = em.createQuery("SELECT c FROM Category AS c").getResultList();
			t.commit();
		} finally {
			if (t.isActive())
				t.rollback();
			em.close();
		}
		return categories;
	}

	@Override
	public void save(Category category) {
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.persist(category);
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
	}

}
