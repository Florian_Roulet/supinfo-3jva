package com.supinfo.supcommerce.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import com.supinfo.supcommerce.dao.ProductDao;
import com.supinfo.supcommerce.entity.Product;

public class JpaProductDao implements ProductDao {

	private EntityManagerFactory emf;
	
	public JpaProductDao(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public void save(Product product) {
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.persist(product);
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
	}

	@Override
	public Product findProductById(Long id) {
		Product product = null;
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			product = em.find(Product.class, Long.valueOf(id));
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
		return product;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllProducts() {
		List<Product> allProducts = null;
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			allProducts = em.createQuery("select p FROM Product AS p").getResultList();
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
		return allProducts;
	}

	@Override
	public void removeProduct(Product product) {
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			em.remove(product);
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
	}

	@Override
	public void removeProduct(Long id) {
		EntityManager em = emf.createEntityManager();;
		EntityTransaction t = em.getTransaction();
		try {
			t.begin();
			Product product = em.find(Product.class, Long.valueOf(id));
			em.remove(product);
			t.commit();
		} finally {
			if (t.isActive()) t.rollback();
			em.close();
		}
	}


}
