package com.supinfo.supcommerce.dao;

import java.util.List;

import com.supinfo.supcommerce.entity.Category;

public interface CategoryDao {

	public Category findById(Long id);
	
	public List<Category> findAll();
	
	public void save(Category category);

}
