<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<a href="<c:out value='${pageContext.request.contextPath}${"/auth/listProduct"}'/>">product list</a>
<a href="<c:out value='${pageContext.request.contextPath}${"/auth/addProduct"}'/>">add a product</a>
<a href='<c:out value="${pageContext.request.contextPath}${'/auth/addCategory'}"/>'>add a category</a>
<c:choose>
	<c:when test="${empty sessionScope.username}">
	<a href="<c:out value='${pageContext.request.contextPath}${"/index.html"}'/>">login</a>
	</c:when>
	<c:otherwise>
	<a href="<c:out value='${pageContext.request.contextPath}${"/logout"}'/>">logout</a>
	</c:otherwise>
</c:choose>