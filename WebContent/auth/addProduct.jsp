<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="../header.jsp" />
	<form action="/SupCommerce/auth/addProduct" method="POST">
		<fieldset>
    		<legend>Add a product : </legend>
			<label for=nameInput>name : </label>
			<input type="text" id="nameInput" name="name"/>
			<label for=contentInput>content : </label>
			<input type="text" id="contentInput" name="content"/>
			<label for=priceInput>price : </label>
			<input type="number" id="priceInput" name="price"/>
			<select name="category.id">
				<c:forEach items="${categories}" var="category">
				<option value='<c:out value="${category.id}"/>'><c:out value="${category.name}"/></option>
				</c:forEach>
			</select>
			<input type="submit" value="Ajouter Produit"/>
		</fieldset>
	</form>
	<jsp:include page="../footer.jsp" />
</body>
</html>