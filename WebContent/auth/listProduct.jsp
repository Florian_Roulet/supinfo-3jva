<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="../header.jsp" />
	<h1>Product list : </h1>
	<ul>
		<c:forEach items="${sessionScope.allProducts}" var="product">
		<li>
			<div>
				<a href="<c:out value="${pageContext.request.contextPath}${'/auth/showProduct.jsp?id='}${product.id}"/>"><c:out value="${product.name}"/></a>
			</div>
			<div>
				<c:out value="${product.content}"/>
			</div>
			<div>
				<c:out value="${product.price}"/>
			</div>
			<div>
				<form action="<c:out value="${pageContext.request.contextPath}${'/auth/removeProduct'}"/>" method="POST"> 
					<input type="hidden" name="id" value="<c:out value="${product.id}"/>"/>
					<input type="submit" value="Remove product"/>
				</form>
			</div>
		</li>
		</c:forEach>
	</ul>
	<jsp:include page="../footer.jsp" />
</body>
</html>