<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="../header.jsp" />
	<div>
		id : 
		<c:out value="${product.id}" />
	</div>
	<div>
		name : 
		<c:out value="${product.name}" />
	</div>
	<div>
		content : 
		<c:out value="${product.content}" />
	</div>
	<div>
		price : 
		<c:out value="${product.price}" />
	</div>
	<div>
		category :
		<c:out value="${product.category.name}"></c:out>
	</div>
	<jsp:include page="../footer.jsp" />
</body>
</html>